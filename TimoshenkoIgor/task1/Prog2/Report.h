#ifndef REPORT_H
#define REPORT_H
#include <string>
using namespace std;

int print_sensors (string);
int print_report (string, int);
#endif // REPORT_H