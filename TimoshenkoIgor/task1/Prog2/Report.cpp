#include "Report.h"
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int print_sensors (string file) 
{
    string buff, sensors, work;
    int count = 0;
    ifstream fin(file);
    
    while(1)
    {
        fin >> buff;
        if (buff == "Model:")
        {
            fin >> buff;
            sensors = sensors + buff + " ";
            count ++;
        }
        if (buff == "WorkHours:")
        {
            fin >> buff;
            work = work + buff + "    ";
        }
        if (fin.eof()) 
        {
            break;
        }
    }
    fin.close();
    cout <<"Sensors: " << sensors << endl << "Worktime: " << work << endl;
    return count;
}

int print_report (string file, int count) 
{
    string buff;
    ifstream fin(file);

    int numb = 0;
    while (numb <= count)
    {
        cout << "Print Sensor number for take report or break - " 
        << (count+1) << ": " << endl;
        cin >> numb;
        if (numb > count)
        {
            return 1;
        }
        int start = 0;
        int end = 0;
        cout << "Select start work time: " << endl;
        cin >> start;
        cout << "Select end work time: " << endl;
        cin >> end;
        int scan = 0;
        while (1)
        {
            fin >> buff;
            if (buff == "Model:")
            {
                scan++;
                if (scan == numb)
                {
                    fin >> buff;
                    cout << buff << endl;
                    while (1)
                    {
                        fin >> buff;
                        int working = 0;
                        if (buff == "WorkHours:")
                        {
                            fin >> working;
                            if (working < end) 
                            {
                                cout << working << " - end hour work" << endl;
                                break;
                            }
                        }
                        if (buff == "DATA:")
                        {
                            cout << buff << endl;
                            float data[40] = {0};
                            int i = 0;
                            for (i = 0; i <= (end-start); i++)
                            {
                                fin >> buff;
                                data[i] = stof(buff);                                
                            }
                            float max = data[0];
                            float min = data[0];
                            float med = data[0];
                            for (i = 0; i <= (end-start); i++)
                            {
                                if (data[i] > max)
                                {
                                    max = data[i];
                                }
                                else if (data[i] < min)
                                {
                                    min = data[i];
                                }
                                med += data[i];
                            }
                            med = med/(end-start);
                            cout << " Min: " << min << " Max: " << max 
                            << " Med: " << med << endl;
                            break;
                        }
                    }
                }
            }

            if (scan == numb)
            {
                fin.clear();
                fin.seekg(0);
                break;
            } 
        }
        fin.clear();
        fin.seekg(0);
    }
    fin.close();
}