#include "sensor.h"
#include <time.h>
#include <stdlib.h>
#include <string>

using namespace std;

Sensor::Sensor(string name,string coord)
{
    Sensor::set_name(name);
    Sensor::set_coord(coord);
}

void Sensor::set_name(string sens_name)
{
    Sensor::name = sens_name;    
}

string Sensor::get_name()
{
    return Sensor::name;
}

void Sensor::set_coord(string sens_coord)
{
    Sensor::coord = sens_coord;
}

string Sensor::get_coord()
{
    return Sensor::coord;
}

float Sensor::temp()
{
    return (-5 + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/10)));
}

float Sensor::humidity()
{
    return (30 + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/30)));
}

string Sensor::set_time()
{
    time_t CurrentTime = time(NULL);
    return ctime(&CurrentTime);
}
