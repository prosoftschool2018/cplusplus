#ifndef SENSOR_H
#define SENSOR_H
#include <string>
using namespace std;
class Sensor {
   public:
      Sensor(string, string);
      void set_name(string);
      string get_name();
      void set_coord(string);
      string get_coord(); 
      float temp ();
      float humidity ();
      string set_time ();
   private:
      string name;
      string coord;
};

#endif // SENSOR_H