#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include "sensor.h"

using namespace std;

int main(int argc, char *argv[])
{
    string name_s;
    string coord_s;
    int numb;
    int work;
    cout << "How much will be sensors?" << endl;
    cin >> numb;
    for(int i = 1; i <= numb;i++)
    {
        cout << "Name Sensor (take ; and Enter): ";
        getline(std::cin, name_s, ';');
        cout << "Coord Sensor (take ; and Enter): ";
        getline(std::cin, coord_s, ';');
        Sensor *sensor = new Sensor(name_s, coord_s);
        ofstream fout("Sensor.txt", ios::app);
        fout << "Model:" << sensor->get_name() << endl; 
        fout << "Coord:" << sensor->get_coord() << endl;
        fout << "Date:" << endl << sensor->set_time();
        cout << "How many hours work sensor: " << endl;
        cin >> work;
        fout << "WorkHours:" << endl << work << endl;
        fout << "DATA:" << endl;
        int type;
        cout << "How type of sensor, 1 - Temp, 2 - Humid: " << endl;
        cin >> type;
        if (type == 1) 
        {
            for (int j = 1; j <= work; j++)
            {
                fout << sensor->temp() << endl;
            }
            fout<<endl;
        }
        else if (type == 2)
        {
            for(int j = 1; j <= work; j++)
            {
               fout << sensor->humidity() << endl;
            }
            fout<<endl;
        }
        delete sensor;
        fout.close();
    } 
    return 0;
}