#include <sensors.h>
#include <QTextStream>
#include <QString>
#include <QDate>
#include <QtSql/QSqlDatabase>
#include <QStandardPaths>
#include <QCoreApplication>

int DB_connect(QSqlDatabase &db) {
    int appNameLength = QCoreApplication::applicationName().length();
    QTextStream(stdout) << "Connection to DB... ";
    int check = 0;
    db = QSqlDatabase::addDatabase("QSQLITE");
    QStringList listOfPaths = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation);
    QString path = listOfPaths[0].remove(listOfPaths[0].length() - appNameLength, appNameLength) + "Qt/Sensors.db";
    db.setDatabaseName(path);
    check = db.open();
    if (check)
        QTextStream(stdout) << "Success\n";
    else
        QTextStream(stdout) << "Error\n";
    return check;
}

QDate settingDate() {
    QString d;
    QTextStream(stdin) >> d;
    QStringList temp = d.split('.');
    int dateValues[3];
    for (int i = 0; i < 3; i++) {
        dateValues[i] = temp[i].toInt();
    }
    QDate date = QDate(dateValues[2], dateValues[1], dateValues[0]);
    return date;
}
