#include <QCoreApplication>
#include <QtSql/QSqlDatabase>
#include <QSqlQuery>
#include <QDate>
#include <QTextStream>
#include <QRandomGenerator>
#include <QDir>
#include <sensors.h>
#include <somefunctions.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    //Подключение к ДБ
    QSqlDatabase db;
    int check = DB_connect(db);
    if (!check)
        return a.exec();

    QSqlQuery query;
    while(1) {
        QTextStream(stdout) << "\t\t\t\tList of sensors\n\n";
        QTextStream(stdout) << "number\t\t model\t\t coordinates\t\t install date\n\n";
        query.exec("SELECT number, model, coordinates, installationDate FROM SensorsList");
        while (query.next()) {
            QTextStream(stdout) << query.value(0).toString() << "\t\t";
            QTextStream(stdout) << query.value(1).toString() << "\t\t";
            QTextStream(stdout) << query.value(2).toString() << "\t\t";
            QTextStream(stdout) << query.value(3).toString() << "\t\t";
            QTextStream(stdout) << "\n\n";
        }

        //Выбор конкретного датчика
        QTextStream(stdout) << "to select some sensor, enter its number\n";
        QString answer;
        QTextStream(stdin) >> answer;

        query.exec(QString("SELECT number, model, coordinates, installationDate FROM SensorsList "
                           "WHERE number = '%1'").arg(answer));
        query.next();
        Sensors sens = Sensors(query.value(3).toString(), query.value(2).toString(),
                               query.value(1).toString(), query.value(0).toInt());

        //Задание диапазона дат
        QTextStream(stdout) << "Set dates for selection of measurments like dd.mm.yyyy\n";
        QTextStream(stdout) << "Set date of start\n";
        QDate dateStart = settingDate();
        QTextStream(stdout) << "Set date of end\n";
        QDate dateEnd = settingDate();

        query.exec(QString("SELECT number FROM temperatureMeasurments "
                           "WHERE date = '%1' OR date = '%2'").arg(
                           dateStart.toString("dd.MM.yyyy"),dateEnd.toString("dd.MM.yyyy")));

        //номер измерения начала и конца
        query.next();
        QString numberStart = query.value(0).toString();
        query.next();
        QString numberEnd = query.value(0).toString();

        QTextStream(stdout) << QString("\nfor\n Range:\n\t %1  -  %2\n Sensor:\n\t model: %3\n\t coordinates: %4\n\t install date: %5\n\n").arg(
                                       dateStart.toString("dd.MM.yyyy"),dateEnd.toString("dd.MM.yyyy"),
                                       sens.model,sens.coordinates,sens.installDate);

        query.exec(QString("SELECT S%1 FROM temperatureMeasurments "
                           "WHERE number BETWEEN '%2' AND '%3'").arg(
                           QString::number(sens.number), numberStart, numberEnd));

        sens.getStatistics(numberEnd.toInt() - numberStart.toInt(),query, "temperature");

        query.exec(QString("SELECT S%1, number FROM wetnessMeasurments "
                           "WHERE number BETWEEN '%2' AND '%3'").arg(
                           QString::number(sens.number), numberStart, numberEnd));

        sens.getStatistics(numberEnd.toInt() - numberStart.toInt(),query, "wetness");

        QTextStream(stdout) << "another query? yes/no \n";
        QString cont;
        QTextStream(stdin) >> cont;
        if (cont != "yes")
            break;
    }


    return a.exec();
}
