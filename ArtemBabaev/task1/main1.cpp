#include <QCoreApplication>
#include <QtSql/QSqlDatabase>
#include <QSqlQuery>
#include <QDate>
#include <QTextStream>
#include <QRandomGenerator>
#include <QDir>
#include <QStandardPaths>
#include <somefunctions.h>
#include <sensors.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    //Подключение к ДБ
    QSqlDatabase db;
    int check = DB_connect(db);
    if (!check)
        return a.exec();

    //Задание диапазона дат
    QTextStream(stdout) << "Set dates for generation of measurments like dd.mm.yyyy\n";
    QTextStream(stdout) << "Set date of start\n";
    QDate dateStart = settingDate();
    QTextStream(stdout) << "Set date of end\n";
    QDate dateEnd = settingDate();

    //Контрольное подтверждение
    QTextStream(stdout) << dateStart.daysTo(dateEnd) << " rows of values will be entered into the DB. Continue? yes/no \n";
    QString answer;
    QTextStream(stdin) >> answer;
    if (answer != "yes")
        return a.exec();

    //Необходимые переменные
    QSqlQuery query;
    const int numberOfSensors = 9;
    Sensors someSensor = Sensors();
    QRandomGenerator gen;

    while (dateStart < dateEnd) {
        QString ValuesOfTempSens = someSensor.formingValuesForTempQuery(numberOfSensors, gen);
        QString ValuesOfWetSens = someSensor.formingValuesForWetQuery(numberOfSensors, gen);

        //Запросы в БД
        query.exec(QString("INSERT INTO temperatureMeasurments (S1,S2,S3,S4,S5,S6,S7,S8,S9,date) "
                           "VALUES (%1,'%2')").arg(ValuesOfTempSens, dateStart.toString("dd.MM.yyyy")));

        query.exec(QString("INSERT INTO wetnessMeasurments (S1,S2,S3,S4,S5,S6,S7,S8,S9,date) "
                           "VALUES (%1,'%2')").arg(ValuesOfWetSens, dateStart.toString("dd.MM.yyyy")));

        //Итератор цикла
        dateStart = dateStart.addDays(1);
    }

    QTextStream(stdout) << "End of program";
    return a.exec();
}


