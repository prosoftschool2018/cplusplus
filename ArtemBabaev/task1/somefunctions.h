#ifndef SOMEFUNCTIONS_H
#define SOMEFUNCTIONS_H
#include <QDate>
#include <QSqlDatabase>

//Подключение к базе данных
int DB_connect(QSqlDatabase &db);

//Установка даты
QDate settingDate();

#endif // SOMEFUNCTIONS_H
