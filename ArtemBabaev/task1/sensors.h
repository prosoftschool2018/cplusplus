#ifndef SENSORS_H
#define SENSORS_H
#include <QString>
#include <QRandomGenerator>
#include <QSqlQuery>
#include <QtSql/QSqlDatabase>
#include <QTextStream>

class Sensors {
public:
    Sensors(QString installDate, QString coordinates, QString model, int number) {
        this->installDate = installDate;
        this->coordinates = coordinates;
        this->model = model;
        this->number = number;
    }
    Sensors(){}
    QString installDate;
    QString coordinates;
    QString model;
    int number = 0;
    QString formingValuesForTempQuery(int iter, QRandomGenerator &gen) {
        QString values = "";
        for (int i = 0; i < iter; i++) {
            values += QString("'%1',").arg(getRandomTemp(gen));
        }
        values.remove(values.length() - 1,1);
        return values;
    }
    QString formingValuesForWetQuery(int iter, QRandomGenerator &gen) {
        QString values = "";
        for (int i = 0; i < iter; i++) {
            values += QString("'%1',").arg(getRandomWet(gen));
        }
        values.remove(values.length() - 1,1);
        return values;
    }
    void getStatistics(int iter, QSqlQuery &query, QString name) {
        ++iter;
        int max = 0;
        int min = 100;
        float average = 0;
        while (query.next()) {
            int temp = query.value(0).toInt();
            if (max < temp)
                max = temp;
            if (min > temp)
                min = temp;
            average += static_cast<float>(temp * 1.0f / iter);
        }

        QTextStream(stdout) << QString("min %4 = %1\n"
                                       "max %4 = %2\n"
                                       "average %4 = %3\n\n").arg(
                                       QString::number(min),QString::number(max),QString::number(average), name);
    }

protected:
    int getRandomTemp(QRandomGenerator &gen) {
        int value = static_cast<int>(20 + ((gen.generate() * 23.0f) / gen.max()));
        return value;
    }
    int getRandomWet(QRandomGenerator &gen) {
        int value = static_cast<int>(((gen.generate() * 100.0f) / gen.max()));
        return value;
    }
};

#endif // SENSORS_H
