#include "value.h"
#include "sensor.h"

Value::Value()
{

}

void WriteData(QTextStream &stream,Value v)
{
    stream<<v.sensor.data()->model<<" "<<v.humidity<<" "
         <<v.temperature<<" "<<v.sensor.data()->x<<" "
        <<v.sensor.data()->y<<" "<<v.sensor.data()->placementDate.day()<<" "
       <<v.sensor.data()->placementDate.month()<<" "
      <<v.sensor.data()->placementDate.year()<<" "<<v.time<<endl;
}

Value generateRandomValue(QSharedPointer<Sensor> sensor)
{
    Value v;
    v.time=abs(rand());
    v.humidity=rand()%100;
    v.temperature=(rand()%1000)/13;
    v.sensor=sensor;
    return  v;
}
