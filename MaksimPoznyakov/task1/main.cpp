#include <QCoreApplication>
#include "sensor.cpp"
#include <QFile>
#include<QTextStream>
#include <random>
#include "value.cpp"
#include <iostream>

const int sensorsCount = 100;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    setlocale(LC_CTYPE, "Russian");
    QFile file("data.txt");
    file.open(QIODevice::ReadWrite);
    QTextStream stream(&file);
    QTextStream qtin(stdin);
    QTextStream qtout(stdout);
    int dataCount = 0;
    for(int i = 0; i < sensorsCount; ++i)
    {
       Sensor s = generateRandomSensor();
       auto ptr = QSharedPointer<Sensor>::create(s);
       int sensorDataCount = rand()%10 + 1;
       for(int j = 0; j < sensorDataCount; ++j)
       {
           Value v = generateRandomValue(ptr);
           WriteData(stream,v);
       }
       dataCount += sensorDataCount;
    }
    QString prevSensor = "";
    for(int i = 0; i < sensorsCount; ++i)
    {
        QString sensorName;
        stream>>sensorName;
        if(sensorName!=prevSensor)
        {
           // std::cout<<qPrintable(sensorName)<<endl;
            qtout<<sensorName<<endl;
            prevSensor = sensorName;
        }
        stream.readLine();
    }
    qtout<<"Введите модель датчика: ";
    QString sensorModel;
    qtin>>sensorModel;
    qtout<<"Введите дату начала {DD MM YY}"<<endl;
    int day,month,year;
    qtin>>day>>month>>year;
    QDate startDate(year,month,day);
    char skip;
    qtin>>skip;
    qtout<<"Введите время начала "<<endl;
    unsigned long long startTime;
    qtin>>startTime;
    qtout<<"Введите дату конца {DD MM YY}"<<endl;
    qtin>>day>>month>>year;
    QDate endDate(year,month,day);
    qtin>>skip;
    qtout<<"Введите время конца "<<endl;
    unsigned long long endTime;
    qtin>>endTime;

    int sensorsCount = 0;
    float minTemp = INT_MAX;
    int averageTemp = 0;
    float maxTemp=0;
    float minHum = INT_MAX;
    int averageHum = 0;
    float maxHum=0;
    for(int i=0;i<dataCount;++i)
    {
        Sensor sensor;
        Value val;
        val.sensor = QSharedPointer<Sensor>::create(sensor);
        ReadSensor(qtin,sensor,val);
        if(sensor.model != sensorModel)
            continue;
        if(val.time>=startTime && val.time<=endTime &&
           sensor.placementDate>=startDate && sensor.placementDate<=endDate)
        {
            minTemp= minTemp < val.temperature ? minTemp : val.temperature;
            minHum= minTemp < val.humidity ? minTemp : val.humidity;
            averageTemp+=val.temperature;
            averageHum+=val.humidity;
            maxTemp= maxTemp > val.temperature ? maxTemp : val.temperature;
            maxHum= maxHum > val.humidity ? maxHum : val.humidity;
        }

    }
    averageTemp = sensorsCount!=0 ? averageTemp / sensorsCount : 0;
    averageHum = sensorsCount!=0 ? averageHum / sensorsCount : 0;

    qtout<<"Минимальная температура: "<<minTemp<<endl;
    qtout<<"Средняя температура: "<<averageTemp<<endl;
    qtout<<"Максимальная температура: "<<minTemp<<endl;

    qtout<<"Минимальная влажность: "<<minHum<<endl;
    qtout<<"Средняя влажность: "<<averageHum<<endl;
    qtout<<"Максимальная влажность: "<<minHum<<endl;

    file.close();
    return 0;
    //return a.exec();
}
