#ifndef VALUE_H
#define VALUE_H
#include<QSharedPointer>
#include<sensor.h>

class Value
{
public:
    Value();
    float temperature;
    float humidity;
    unsigned long long time;
    QSharedPointer<Sensor> sensor;
};

#endif // VALUE_H
