#include "sensor.h"
#include "value.h"

Sensor::Sensor()
{

}

Sensor generateRandomSensor()
{
    Sensor s;
    s.model=static_cast<char>(rand()%100+42);
    //s.time=abs(rand());
    s.x=rand();
    s.y=rand();
    //s.humidity=rand();
   // s.temperature=(rand()%1000)/13;
    s.placementDate=QDate(rand()%20+2000,rand()%12,rand()%30);
    return s;
}

void ReadSensor(QTextStream &stream,Sensor &sensor,Value &value)
{
    stream>>sensor.model>>value.humidity>>
         value.temperature>>sensor.x>>sensor.y;
    int day,month,year;
    stream>>day>>month>>year;
    sensor.placementDate=QDate(year,month,day);
    stream>>value.time;
}
