#ifndef SENSOR_H
#define SENSOR_H
#include<QString>
#include<QDate>
#include<QTextStream>


class Sensor
{
public:
    Sensor();
    double x;
    double y;
    QString model;
    QDate placementDate;
    void WriteFile(QTextStream &s,Sensor sensor);
    Sensor ReadFile(QTextStream &stream);
};

#endif // SENSOR_H
