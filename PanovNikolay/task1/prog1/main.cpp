#include <iostream>
#include "sensor.h"
#include "windows.h"
#include "dataBase.h"

int main(int argc, char* argv[]) {
    Sensor* temperSensor = new TemperatureSensor;
    Sensor* humiditySensor = new HumiditySensor;
    dataBase sensorsDataBase;
    sensorsDataBase.open("sens.db");
    sensorsDataBase.createTable({{"date","DATETIME"}, {"TempSensor","REAL"}, {"HumSensor","REAL"}});
    sensorsDataBase.close();
    unsigned long int recordTimeInSecond;
    std::cout << "Enter recording time in seconds" << std::endl;
    std::cin >> recordTimeInSecond;
    for (size_t i = 0; i < recordTimeInSecond; i++) {
        sensorsDataBase.open("sens.db");
        sensorsDataBase.insertInTable({{"date","datetime('now')"}, 
                                       {"TempSensor", std::to_string(temperSensor->getData())}, 
                                       {"HumSensor",  std::to_string(humiditySensor->getData())}}); 
        sensorsDataBase.close();
        Sleep(1000);                                   
    }
    delete temperSensor;
    delete humiditySensor;   
    return 0;
}
