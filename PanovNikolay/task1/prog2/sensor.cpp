#include "sensor.h"

double HumiditySensor::getData() {
    humidity = std::rand() % 100;
    return humidity;
}

double TemperatureSensor::getData() {
    temperature += (std::rand() % 10)/10.0 - 0.45;
    if (temperature > 40)
        temperature = 40;
    if (temperature < -30)
        temperature = -30;
    return temperature;
}

