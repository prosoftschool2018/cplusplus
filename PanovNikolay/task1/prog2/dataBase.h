#ifndef DATABASE_H
#define DATABASE_H
#include"sqlite3.h"
#include <iostream>
#include<string>
#include<map>
#include<vector>

class dataBase {
public:
    void open(std::string fileName);
    void createTable(std::map<std::string, std::string> colsAndType);
    void insertInTable(std::map<std::string, std::string> colsAndData);
    void close(void);
    void getColumnsName(void);
    void printSensorName(void);
    void printValFromRange(std::string cols, std::string typeVal, std::string dateTimeBeg, std::string dateTimeEnd);
    static std::vector<std::string> nameCols;
private:
    static int callBackNameCols(void *data, int argc, char **argv, char **azColName);
    static int callBackFindVal(void *NotUsed, int argc, char **argv, char **azColName);
    static int callBackEmpty(void *NotUsed, int argc, char **argv, char **azColName);
    sqlite3 *db;
    char* zErrMsg;

};
#endif // DATABASE_H
