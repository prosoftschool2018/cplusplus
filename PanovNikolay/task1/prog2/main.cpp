#include <iostream>
#include "sensor.h"
#include "dataBase.h"
#include <vector>

int main(int argc, char* argv[]) {
    dataBase sensorsDataBase;
    sensorsDataBase.open("sens.db");
    sensorsDataBase.getColumnsName();

    std::cout << "Select sensor" << std::endl;
    sensorsDataBase.printSensorName();
    int numSensor;
    std::cin >> numSensor;
    while((numSensor == 0)||(numSensor > dataBase::nameCols.size()-1))
    {
        std::cout << "Sensor does not exist" << std::endl;
        std::cout << "Select sensor" << std::endl;
        std::cin >> numSensor;
    }
    std::string nameSensor = dataBase::nameCols[numSensor-1];
    std::cout << nameSensor <<" selected" << std::endl;
    std::cout << "Enter begin datetime (e.g. 2019-03-13 05:31:35)" << std::endl;
    std::string _date;
    std::string _time;
    std::cin >> _date;
    std::cin >> _time;
    std::string dateTimeBeg = _date + " " + _time;
    std::cout << "Enter end datetime (e.g. 2019-03-13 05:43:06)" << std::endl;
    std::cin >> _date;
    std::cin >> _time;
    std::string dateTimeEnd = _date + " " +  _time;
    std::cout << "Sensor: "<< nameSensor << std::endl;
    std::cout << "Time range: "<< dateTimeBeg << " - " << dateTimeEnd << std::endl;
    std::cout << "Max value: ";
    sensorsDataBase.printValFromRange(nameSensor, "MAX", dateTimeBeg, dateTimeEnd);
    std::cout << "Min value: ";
    sensorsDataBase.printValFromRange(nameSensor, "MIN", dateTimeBeg, dateTimeEnd);
    std::cout << "Average value: ";
    sensorsDataBase.printValFromRange(nameSensor, "AVG", dateTimeBeg, dateTimeEnd);
    sensorsDataBase.close();
    return 0;
}
