#include "sensor.h"
#include "utils.h"
#include <iostream>
#include <string>

using namespace std;

Sensor::Sensor(
    unsigned int id,
    const SensorType type,
    const string& model,
    double longitude,
    double latitude,
    const char* instTime)
{
    sens_id = id;
    sens_type = type;
    sens_model = model;
    sens_longitude = longitude;
    sens_latitude = latitude;
    sens_instTime = cstrToTime(instTime);
}

short int Sensor::getRandomVal() const
{
    switch (sens_type) {
        case SensorType::temp:
        return getRndTemp();
        break;
    case SensorType::humid:
        return getRndHumidity();
        break;
    default:
        throw "Unknown sensor type index";
    }
}

void Sensor::printData() const
{
    cout << "id: " << sens_id << endl;
    cout << "type: " << SENSOR_TYPES[sens_type] << endl;
    cout << "model: " << sens_model << endl;
    cout << "latitiude: " << sens_latitude << endl;
    cout << "longitude: " << sens_longitude << endl;
    cout << "instTime: " << timeToStr(&sens_instTime) << endl;
}

short int Sensor::getRndTemp() {
    return getRndFromRange<short int>(-60, 60);
}

short int Sensor::getRndHumidity() {
    return getRndFromRange<short int>(0, 100);
}

void SensorStat::addVal(short int val)
{
    if (numOfVals != 0) {
        minVal = min<short int>(minVal, val);
        maxVal = max<short int>(maxVal, val);
        sum += val;
    } else {
        minVal = val;
        maxVal = val;
    }
    numOfVals++;
}

void SensorStat::printStat() const
{
    if (numOfVals == 0)
    {
        cout << "Min: N/A" << endl;
        cout << "Max: N/A" << endl;
        cout << "Average: N/A" << endl;
    } else {
        cout << "Min: " << minVal << endl;
        cout << "Max: " << maxVal << endl;
        cout << "Average: " << sum / numOfVals << endl;
    }
}

void SensorStat::resetStat()
{
    minVal = 0;
    maxVal = 0;
    sum = 0;
    numOfVals = 0;
}