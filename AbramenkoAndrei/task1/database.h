#ifndef CPLUSPLUS_ABRAMENKOANDREI_TASK1_DATABASE_H_
#define CPLUSPLUS_ABRAMENKOANDREI_TASK1_DATABASE_H_

#include "lib/sqlite3/sqlite3.h"
#include <string>

using namespace std;

// Typedef for sqlite3_exec callback
typedef int (*execSQLcallback) (void*, int, char**, char**);

class Database
{
    private:
    string filename;
    sqlite3* db;

    public:
    Database(const string& fname);

    const string& getFilename() const;

    bool open();

    bool close();

    bool exec(const string& sql, execSQLcallback callback, string& mes);
};

#endif