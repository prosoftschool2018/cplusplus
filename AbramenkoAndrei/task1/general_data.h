#ifndef CPLUSPLUS_ABRAMENKOANDREI_TASK1_GENERAL_DATA_H_
#define CPLUSPLUS_ABRAMENKOANDREI_TASK1_GENERAL_DATA_H_

#include <string>

#include "sensor.h"
#include "utils.h"

using namespace std;

const char* const FILENAME = "sensors_vals.db";

const size_t NUMB_OF_SENSORS = 4;

const Sensor SENSORS[NUMB_OF_SENSORS] = {
    Sensor(0,   SensorType::temp,   "Model0",   100.0,  100.0,  "2008-08-08 08:08:08"),
    Sensor(1,   SensorType::humid,  "Model1",   200.0,  200.0,  "2009-09-09 09:09:09"),
    Sensor(2,   SensorType::temp,   "Model2",   300.0,  300.0,  "2010-10-10 10:10:10"),
    Sensor(3,   SensorType::humid,  "Model3",   400.0,  400.0,  "2011-11-11 11:11:11")
};

const time_t START_TIME = cstrToTime("2011-11-11 11:11:20");
const time_t END_TIME = cstrToTime("2011-11-11 11:11:40");

#endif