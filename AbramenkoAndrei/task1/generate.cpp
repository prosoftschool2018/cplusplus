#include <iomanip>
#include <iostream>
#include <string>

#include "database.h"
#include "general_data.h"
#include "utils.h"

using namespace std;

int main(int argc, char* argv[])
{
    static const short int TIME_COLUMN_WIDTH = 20;
    static const short int VAL_COLUMN_WIDTH = 7;

    // Open database
    Database db = Database(FILENAME);
    if(!db.open())
    {
        puts("Fail open database");
        return EXIT_FAILURE;
    }

    // Drop an old table
    string sql = "DROP TABLE IF EXISTS SENSORS_VALS";
    string message;
    if(!db.exec(sql, nullptr, message))
    {
        puts("Fail drop table:");
        cout << message << endl;
        return EXIT_FAILURE;
    }

    // Create table
    sql = "CREATE TABLE SENSORS_VALS (TIME INT PRIMARY KEY NOT NULL, ";
    cout << setw(TIME_COLUMN_WIDTH) << "Time";
    string column;
    for (Sensor sensor : SENSORS)
    {
        column = to_string(sensor.sens_id);
        sql += " ID" + column + " INT,";
        column =  "id" + column;
        cout << setw(VAL_COLUMN_WIDTH) << column;
    }
    cout << endl;
    sql[sql.length() - 1] = ')';
    sql += ';';
    if (!db.exec(sql, nullptr, message))
    {
        puts("Fail create table:");
        cout << message << endl;
        return EXIT_FAILURE;
    }

    // Filling table, print time and values
    time_t valsTime = START_TIME;
    while (valsTime <= END_TIME)
    {
        // Forming SQL statement
        sql = "INSERT INTO SENSORS_VALS VALUES(";
        cout << setw(TIME_COLUMN_WIDTH) << timeToStr(&valsTime);
        sql += to_string(valsTime) + ", ";
        for (unsigned int i = 0; i < NUMB_OF_SENSORS; i++)
        {
            if (SENSORS[i].sens_instTime <= valsTime)
            {
                column = to_string(SENSORS[i].getRandomVal());
                cout << setw(VAL_COLUMN_WIDTH) << column;
                sql += column;
            } else {
                sql += " NULL";
                cout << setw(VAL_COLUMN_WIDTH) << "NULL";
            }
            sql += ',';
        }
        cout << endl;
        sql[sql.length() - 1] = ')';
        sql += ';';

        // SQL execution
        if(!db.exec(sql, nullptr, message))
        {
            puts("Fail filling table:");
            cout << message << endl;
            return EXIT_FAILURE;
        }
        valsTime++;
    }

    // Close connection to database
    if(!db.close())
    {
        puts("Fail close database");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}