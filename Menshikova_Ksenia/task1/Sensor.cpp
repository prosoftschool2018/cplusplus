#include <string>
#include <cstdlib>
#include <ctime>
#include "Sensor.h"

#define l_y 3000
#define l_mon 12
#define l_d 31

#define l_h 23
#define l_m 60
#define l_s 60

#define start 0
#define start_1 1

using namespace std;
//------------
//������������
//------------
Tem_sensor::Tem_sensor()//����������� �� ���������
{
    type_sensor ='\0';

    cor_sensor.latitude = 0;
    cor_sensor.longitude = 0;

    date_sensor.day =0;
    date_sensor.month =0;
    date_sensor.year = 0;
}

Tem_sensor::Tem_sensor(const std::string& t_s, const coordinates & c_s,
    const install_date & i_s)//����������� �������������
{
    type_sensor = t_s;

    cor_sensor.latitude=c_s.latitude;
    cor_sensor.longitude = c_s.longitude;

    date_sensor.day=i_s.day;
    date_sensor.month = i_s.month;
    date_sensor.year = i_s.year;

    /*������� ������ � �������*/
    insert_into_base();
}

void Tem_sensor::Show()const
{   
    cout << "Type of sensor: ";
    cout <<type_sensor << endl;

    cout << "Coordinates of sensor: ";
    cout <<"latitude = "<<cor_sensor.latitude << endl;
    cout << "longitude = "<<cor_sensor.longitude << endl;
    
    cout << "Sensor installation date : ";
    cout << date_sensor.day <<"."
         << date_sensor.month <<"."
         << date_sensor.year << endl;
}

//callback
static int callback(void *NotUsed, int argc, char **argv, char **azColName)
{
    int i;
    for (i = 0; i<argc; i++)
    {
        std::cout << azColName[i] << " = " << (argv[i] ? argv[i] : "NULL") << "\n";
    }
    std::cout << "\n";
    return 0;
}

/*�������� ��*/
void Create_Base()
{
    //��������� �������
    int error;
    //��������� �� ������
    char *zErrMsg;

    sqlite3 * db_base = 0;

    error=sqlite3_open("Sensor_base.db", &db_base);
    if (error)
    {
        std::cout << "Can't open database: " << sqlite3_errmsg(db_base) << "\n";
    }    

    /* Create SQL statement */
    /*Base_table*/
    const char* _SQLquery = "CREATE TABLE IF NOT EXISTS 'Sensor_base'"\
        "(id INTEGER PRIMARY KEY,model_sensor CHAR(100)"\
        ",latitude REAL,longitude REAL,Date CHAR(10));";
    /* Execute SQL1 statement */
    sqlite3_exec(db_base, _SQLquery, callback, 0, &zErrMsg);

    /*Additional table*/
    _SQLquery = "CREATE TABLE IF NOT EXISTS 'Sensor_testimonies'"\
        "(id INTEGER PRIMARY KEY,model_sensor CHAR(100),"\
        "TIME CHAR(100),testimones REAL);";
    
    /* Execute SQL2 statement */
    sqlite3_exec(db_base, _SQLquery, callback, 0, &zErrMsg);
    
    /*Close BD*/
    sqlite3_close(db_base);   
}

//---------
//���������
//---------
void Generator(Tem_sensor *sensor)
{    
    srand(time(0));

    //------
    //������
    //------
    double Sensor_Value = 0.0;//��������� �������

    int last_value = 150;//�������� �� first_value ...last_value
    int first_value = -30;//
                          
     //���������� ���������                     
    bool flag = 1;//���� ��� ������������� ��������� 1/��� 0-�� ���������� ����� ���������,������������ ������� � �������
    unsigned int time_count = 1;//������� ������
    unsigned int time_generate = 1;//����� ���������                                
                                
     //------
     //������������� ���������
     //------

    time_generate = rand() % 20; //���������� ���������

    //------
    //�������������� �������
    int start_year = sensor->GetYear();

    //����� �������
    //----------
  
    int new_year;
    int new_month;
    int new_day;

    int new_hours;
    int new_minutes;
    int new_seconds;

    std::string time=""; /*YYYY - MM - DD HH : MM:SS*/

    /*��*/
    sqlite3 * db_base = 0; 
    sqlite3_stmt *stmtU = nullptr;
    const wchar_t* _SQLquery = nullptr;

    sqlite3_open("Sensor_base.db", &db_base);

    while (time_generate--)
    {
        new_year = start_year+ rand() % l_y;
        new_month = start_1+rand()%l_mon;
        new_day = start_1+rand()% l_d ;

        new_hours = start+rand() % l_h ;
        new_minutes = start+rand() % l_m ;
        new_seconds = start+rand() %l_s ;

        _SQLquery = L"INSERT INTO 'Sensor_testimonies'(model_sensor,TIME,testimones) "\
            "VALUES(?,?,?);";
        sqlite3_prepare16(db_base, _SQLquery, -1, &stmtU, 0);

        if (flag)
        {
            //���������� ��������� �������
            Sensor_Value = double(rand()) / RAND_MAX*(last_value - first_value) + first_value;
        }
       
        flag = rand() % 2;

        time = to_string(new_year);
       
        //______������� � ������
        //-----�����
        if (new_month < 10)
        {
            time += " - 0" + to_string(new_month);
        }
        else
        {
            time += " - " + to_string(new_month);
        }
        //-------

        //-----����
        if (new_day  < 10)
        {
            time += " - 0" + to_string(new_day);
        }
        else
        {
            time += " - " + to_string(new_day);
        }
        //-------

        //-----����
        if (new_hours  < 10)
        {
            time += " 0" + to_string(new_hours);
        }
        else
        {
            time += " " + to_string(new_hours);
        }
        //-------
     
        //-----������
        if (new_minutes  < 10)
        {
            time += " : 0" + to_string(new_minutes);
        }
        else
        {
            time += " : " + to_string(new_minutes);
        }
        //------- 
       
        //-----�������
        if (new_seconds  < 10)
        {
            time += ":0" + to_string(new_seconds);
        }
        else
        {
            time += ":" + to_string(new_seconds);
        }
        //-------

        //________________

        //------
        //INSERT
        //------
        sqlite3_bind_text(stmtU, 1, sensor->GetNameSensor().c_str(), -1, 0);
        sqlite3_bind_text(stmtU, 2, time.c_str(), -1, 0);
        sqlite3_bind_double(stmtU, 3, Sensor_Value);
        
        sqlite3_step(stmtU);
        sqlite3_reset(stmtU);

        time ="";
        time_count++;
    }
    sqlite3_reset(stmtU);
    sqlite3_finalize(stmtU);//���������� ������ �������

    /*Close BD*/
    sqlite3_close(db_base);
}

//-----
//MAX AVG MIN
double Tem_sensor::Max(const string& time_start, const string& time_end)const

{ 
    sqlite3_stmt *stmtU;
    sqlite3 * db = 0;
    sqlite3_open("Sensor_base.db", &db);

    const wchar_t* _SQLquery;

    _SQLquery = L"SELECT max(testimones) FROM Sensor_testimonies WHERE model_sensor=?"\
        "AND (TIME BETWEEN ? AND ?);";

    sqlite3_prepare16(db, _SQLquery, -1, &stmtU, 0);

    sqlite3_bind_text(stmtU, 1, this->type_sensor.c_str(), -1, 0);
    sqlite3_bind_text(stmtU, 2, time_start.c_str(), -1, 0);
    sqlite3_bind_text(stmtU, 3, time_end.c_str(), -1, 0);

    double Max;

    while (sqlite3_step(stmtU) == SQLITE_ROW)
    {
        Max = sqlite3_column_double(stmtU, 0);
    }

    sqlite3_reset(stmtU);

    sqlite3_finalize(stmtU);
    sqlite3_close(db);
    return Max;
}
double Tem_sensor::Middle(const string& time_start, const string& time_end)const
{
    sqlite3_stmt *stmtU;
    sqlite3 * db = 0;
    sqlite3_open("Sensor_base.db", &db);

    const wchar_t* _SQLquery;

    _SQLquery = L"SELECT avg(testimones) FROM Sensor_testimonies WHERE model_sensor=?"\
        "AND (TIME BETWEEN ? AND ?);";
    
    sqlite3_prepare16(db, _SQLquery, -1, &stmtU, 0);

    sqlite3_bind_text(stmtU, 1, this->type_sensor.c_str(), -1, 0);
    sqlite3_bind_text(stmtU, 2, time_start.c_str(), -1, 0);
    sqlite3_bind_text(stmtU, 3, time_end.c_str(), -1, 0);

    double AVG;

    while (sqlite3_step(stmtU) == SQLITE_ROW)
    {
        AVG = sqlite3_column_double(stmtU, 0);
    }

    sqlite3_reset(stmtU);

    sqlite3_finalize(stmtU);
    sqlite3_close(db); 
    return AVG;
}
double Tem_sensor::Min(const string& time_start, const string& time_end)const
{
    sqlite3_stmt *stmtU;
    sqlite3 * db = 0;
    sqlite3_open("Sensor_base.db", &db);

    const wchar_t* _SQLquery;

    _SQLquery = L"SELECT min(testimones) FROM Sensor_testimonies WHERE model_sensor=?"\
        "AND (TIME BETWEEN ? AND ?);";

    sqlite3_prepare16(db, _SQLquery, -1, &stmtU, 0);

    sqlite3_bind_text(stmtU, 1, this->type_sensor.c_str(), -1, 0);
    sqlite3_bind_text(stmtU, 2, time_start.c_str(), -1, 0);
    sqlite3_bind_text(stmtU, 3, time_end.c_str(), -1, 0);

    double Min;

    while (sqlite3_step(stmtU) == SQLITE_ROW)
    {
        Min = sqlite3_column_double(stmtU, 0);
    }

    sqlite3_reset(stmtU);

    sqlite3_finalize(stmtU);
    sqlite3_close(db);
    return Min;
}

//-----
//private
//------
void Tem_sensor::insert_into_base()
{
    /*������� ����->text*/
    string f_str = to_string(date_sensor.year) + "-";//���
    if (date_sensor.month  < 10)//�����
    {
        f_str += "0" + std::to_string(date_sensor.month) + "-";
    }
    else
    {
        f_str += std::to_string(date_sensor.day) + "-";
    }
    f_str += std::to_string(date_sensor.day);

    //-----
    //������� ���������� � ��
    //------
    sqlite3_stmt *stmtU;
    sqlite3 * db_base = 0;
    int result_sq_op;

    result_sq_op=sqlite3_open("Sensor_base.db", &db_base);

    /*��������� ������*/
    if (result_sq_op)
    {
        cout << "Can't open database: " << sqlite3_errmsg(db_base) << "\n";
    }
    const wchar_t* _SQLquery = L"INSERT INTO 'Sensor_base'(model_sensor,latitude,longitude,Date) "\
        "VALUES(?,?,?,?);";

    /*���������� � ���������� �������*/
    sqlite3_prepare16(db_base, _SQLquery, -1, &stmtU, 0);

    sqlite3_bind_text(stmtU, 1, type_sensor.c_str(), -1, 0);
    sqlite3_bind_double(stmtU, 2, cor_sensor.latitude);
    sqlite3_bind_double(stmtU, 3, cor_sensor.longitude);
    sqlite3_bind_text(stmtU, 4, f_str.c_str(), -1, 0);

    sqlite3_step(stmtU);
    sqlite3_reset(stmtU);

    sqlite3_finalize(stmtU);

    /*Close BD*/
    sqlite3_close(db_base);
}

