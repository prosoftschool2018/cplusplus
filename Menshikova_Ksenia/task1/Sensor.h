#include <iostream>
#include <sqlite3.h> 

using namespace std;
#ifndef Sensor
#define Sensor
//------
//���������
//-------
struct coordinates//����������
{
    double latitude;
    double longitude;
};

struct install_date//���� ��������� �������
{
    int day;
    int month;
    int year;
};

//------
//����� ��� �������
//-------
class Tem_sensor
{

private:

   std::string type_sensor;//������ �������

   coordinates cor_sensor;//����������
   
   install_date date_sensor;//���� ���������    

   void insert_into_base();//������� � �������

public:
    //------------
    //������������
    //------------
    
    Tem_sensor();//�� ���������

    Tem_sensor(const std::string& t_s,const coordinates & c_s,
        const install_date & i_s);//����������� �������������
   
    ~Tem_sensor() {}; //����������

    int GetYear()const
    {
        return  date_sensor.year;
    }
      const string & GetNameSensor()const
    {
        return  type_sensor;
    }

    double Max(const string& time_start,const string& time_end)const;
    double Middle(const string& time_start, const string& time_end)const;
    double Min(const string& time_start, const string& time_end)const;

    void Show()const;
    friend void Generator(Tem_sensor * sensor);
   
};

void Create_Base();//�������� ��

#endif // !Tempe_sensor



