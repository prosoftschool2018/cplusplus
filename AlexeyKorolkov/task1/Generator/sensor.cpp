#include "sensor.h"

#include <iostream>

using namespace std;

// обработка запроса SELECT
static int callback(void *NotUsed, int argc, char **argv, char **azColName)
{
   int i;
   for(i=0; i<argc; i++)
       printf("%s:\t%s\n", azColName[i], argv[i] ? argv[i] : "NULL");
   printf("\n");
   return 0;
}

Sensor::Sensor() {
    string query = "SELECT * FROM  sensor_data";
    sqlite3 *db;
    if (sqlite3_open("./data.db", &db) == SQLITE_OK) {
        if (sqlite3_exec(db, query.c_str(), callback, nullptr, nullptr) != SQLITE_OK) {
            cout << sqlite3_errmsg(db) << endl << "Press any key to continue\n";
            sqlite3_close(db);
            getchar();
            exit(EXIT_FAILURE);
        }
    } else {
        cout << sqlite3_errmsg(db) << endl << "Press any key to continue\n";
        sqlite3_close(db);
        getchar();
        exit(EXIT_FAILURE);
    }
    sqlite3_close(db);
}

//Получение даты и времени
void Sensor::get_date(string &__date, string &__time) {
    time_t rawtime;
    struct tm *timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    string _date = to_string(timeinfo->tm_mday) + "." +
             to_string(timeinfo->tm_mon + 1) + "." +
             to_string(timeinfo->tm_year + 1900);

    string _time = to_string(timeinfo->tm_hour) + " : " +
             to_string(timeinfo->tm_min);
    if (timeinfo->tm_hour < 10)
        _time = "0"+_time;
    if (timeinfo->tm_mday < 10)
        _date = "0"+_date;
    __date = _date;
    __time = _time;
}

// Генерация данных для БД
void Sensor::generate(sqlite3 *db) {
    string query = "INSERT INTO data_from_temp_sensor ";
    int measure = 0;
    if (sqlite3_open("./data.db", &db) == SQLITE_OK) {
        string d = "", t = "";
        get_date(d, t);

        // Температура:
        measure = rand() % 50 - 50;
        query += "(date,time,value) VALUES(\'" + d + "\' , \'" + t + "\', " + to_string(measure) + ");";
        if (sqlite3_exec(db, query.c_str(), nullptr, nullptr, nullptr) != SQLITE_OK)
            cout << sqlite3_errmsg(db) << endl;

        // Давление:
        query = "INSERT INTO data_from_pressure_sensor ";
        measure = rand() % 81;
        measure += 700;
        query += "(date,time,value) VALUES(\'" + d + "\' , \'" + t + "\', " + to_string(measure) + ");";
        if (sqlite3_exec(db, query.c_str(), nullptr, nullptr, nullptr) != SQLITE_OK) {
            cout << sqlite3_errmsg(db) << endl << "Press any key to continue\n";
            sqlite3_close(db);
            getchar();
            exit(EXIT_FAILURE);
        }

    } else {
        cout << sqlite3_errmsg(db) << endl << "Press any key to continue\n";
        sqlite3_close(db);
        getchar();
        exit(EXIT_FAILURE);
    }
    sqlite3_close(db);
}
