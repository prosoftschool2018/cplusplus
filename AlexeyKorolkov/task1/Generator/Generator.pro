TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
    sensor.cpp \
    point.cpp

HEADERS += \
    sensor.h \
    point.h

LIBS += \
    libsqlite3
