#ifndef SENSOR_H
#define SENSOR_H

#include <sqlite3.h>
#include <ctime>
#include <string>
#include <iostream>
#include <cstdio>

#include "point.h"

using namespace std;

class Sensor
{
private:
    Point Coordinates; // координаты счетчика
    time_t date_installation; // дата установки
    string model; // модель*/
private:
    void get_date(string &__date, string &__time);
public:
    Sensor();
    void generate(sqlite3 *db); //функция, записывающая данные по искомой величине в БД
};

#endif // SENSOR_H
