#include "sensor.h"

#include <string>

#define N 10

void delay (int ms) {
    long t1 = clock()/CLK_TCK*1000+ms, t2 = 0;
    while ((t2 = clock()/CLK_TCK*1000) < t1);
}

using namespace std;

int main()
{
    sqlite3 *db = nullptr;
    Sensor sens;
    cout << "\nWait, please. Preparing data\n";
    for (int i = 0; i < N; i++) {
        sens.generate(db);
        delay(1000);
    }
    cout << "Success!\n";
    getchar();
    return 0;

}
