#ifndef POINT_H
#define POINT_H

class Point
{
private:
    int X;
    int Y;
public:
    int get_X() {return X;}
    int get_Y() {return Y;}
    void set(int x, int y) {X = x; Y = y;}
};

#endif // POINT_H
