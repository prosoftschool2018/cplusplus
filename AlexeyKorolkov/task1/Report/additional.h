#ifndef ADDITIONAL_H
#define ADDITIONAL_H

#include <iostream>
#include <string>
#include <regex>

using namespace std;

void ddmmyy(string s, int &d, int &m, int &y);
void hhmm(string s, int &h, int &m);

bool check_date(string d);
bool check_time(string t);

#endif // ADDITIONAL_H
