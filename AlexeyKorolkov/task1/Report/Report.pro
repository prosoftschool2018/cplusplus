TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
    additional.cpp

LIBS += \
    libsqlite3

HEADERS += \
    additional.h
