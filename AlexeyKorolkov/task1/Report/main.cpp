#include <iostream>
#include <sqlite3.h>
#include <string>
#include "additional.h"

using namespace std;

static int n_0=0, n_1=0;

static int callback(void *NotUsed, int argc, char **argv, char **azColName)
{
    int max = atoi(argv[0]), min = atoi(argv[0]), mid = 0;
    for (int i=0; i<argc; i++) {
        int t = atoi(argv[i]);
        mid += t;
        if (t > max)
            max = t;
        if (t < min)
            min = t;
    }
    mid /= argc;
    printf("Minimum:\t%d\n", min);
    printf("Medium:\t%d\n", mid);
    printf("Maximum:\t%d\n", mid);
    return 0;
}

static int callback_get_diap(void *NotUsed, int argc, char **argv, char **azColName) {
    n_0 = atoi((argv[0]));
    n_1 = atoi((argv[1]));
    return 0;
}

int main()
{
    sqlite3 *db = nullptr;
    string query = "", d_n="", d_k="", t_n="", t_k="";
    if (sqlite3_open("../../build-Generator-Desktop_Qt_5_11_2_MinGW_32bit-Release/release/data.db", &db)) {
        cout << sqlite3_errmsg(db) << endl << "Press any key ro continue\n";
        getchar();
        sqlite3_close(db);
        exit(EXIT_FAILURE);
    }

    cout << "Enter a beginning date (dd.mm.yyyy):\n";
    getline(cin, d_n);
    cout << "Enter an ending date (dd.mm.yyyy):\n";
    getline(cin, d_k);
    cout << "Enter a beginning time (hh : mm):\n";
    getline(cin, t_n);
    cout << "Enter an ending time (hh : mm):\n";
    getline(cin, t_k);
    bool checked = check_date(d_n) && check_date(d_k) && check_time(t_n) && check_time(t_k);
    int d, m, y, h, min, _d, _m, _y, _h, _min;
    ddmmyy(d_n, d, m, y); hhmm(t_n, h, min);
    ddmmyy(d_k, _d, _m, _y); hhmm(t_k, _h, _min);

    cout << "Enter a type of sensor: temperature or pressure (print t/T or p/P)\n";
    char q;
    cin >> q;
    if (checked == true) {
        switch (q) {
        case 't':
        case 'T':
            query = "SELECT No FROM data_from_temp_sensor WHERE date = " + d_n + ";" +
                    "SELECT No FROM data_from_temp_sensor WHERE date = " + d_k + ";" ;
            if (sqlite3_exec(db, query.c_str(), callback_get_diap, nullptr, nullptr) != SQLITE_OK) {
                cout << sqlite3_errmsg(db) << endl << "Press any key to continue\n";
                sqlite3_close(db);
                getchar();
                exit(EXIT_FAILURE);
            }
            query = "SELECT value FROM data_from_temp_sensor WHERE No BETWEEN "+ to_string(n_0) + " AND " + to_string(n_1)+ ";";
            break;
        case 'p':
        case 'P':
            query = "SELECT No FROM data_from_pressure_sensor WHERE date = " + d_n + " AND time = "+t_n+";" +
                    "SELECT No FROM data_from_pressure_sensor WHERE date = " + d_k + " AND time = "+t_n+";" ;
            if (sqlite3_exec(db, query.c_str(), callback_get_diap, nullptr, nullptr) != SQLITE_OK) {
                cout << sqlite3_errmsg(db) << endl << "Press any key to continue\n";
                sqlite3_close(db);
                getchar();
                exit(EXIT_FAILURE);
            }
            query = "SELECT value FROM data_from_temp_sensor WHERE No BETWEEN "+ to_string(n_0) + " AND " + to_string(n_1)+ ";";

            break;
        default:
            cout << "Wrong identificator of sensor!\n";
            break;
        }
        if (sqlite3_exec(db, query.c_str(), callback, nullptr, nullptr) != SQLITE_OK) {
            cout << sqlite3_errmsg(db) << endl << "Press any key to continue\n";
            sqlite3_close(db);
            getchar();
            exit(EXIT_FAILURE);
        }
    }

    sqlite3_close(db);
    getchar();
    return 0;
}
