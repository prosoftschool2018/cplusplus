#include "additional.h"

void ddmmyy(string s, int &d, int &m, int &y) {
    d = atoi(s.substr(0, 2 ).c_str());
    m = atoi(s.substr(3, 2 ).c_str());
    y = atoi(s.substr(6, 4 ).c_str());
}

void hhmm(string s, int &h, int &m) {
    h = atoi(s.substr(0, 2 ).c_str());
    m = atoi(s.substr(5, 2 ).c_str());
}

bool check_date(string d) {
    regex re_d("(^\\d{2}).(\\d{2}).(\\d{4}$)");
    smatch m;
    if (regex_match(d, m, re_d) > 0)
        return true;
    else return false;
}

bool check_time(string t) {
    regex re_t("(^\\d{2}) : (\\d{2}$)");
    smatch m;
    if (regex_match(t, m, re_t) > 0)
        return true;
    else return false;
}
