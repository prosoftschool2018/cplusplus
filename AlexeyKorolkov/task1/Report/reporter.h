#ifndef REPORTER_H
#define REPORTER_H

#include <sqlite3.h>

enum Quantity  {
    TEMPERATURE,
    PRESSURE
};

class Reporter
{
public:
    Reporter();
    void Load(sqlite3 *db, Quantity type);
};

#endif // REPORTER_H
