#include <QCoreApplication>
#include<iostream>
#include <QtSql>
#include<QString>
#include <QTextStream>
#include "QSqlQuery"
#include "C:\Users\maks\cplusplus\PanshinMaxim\prog1\base_date.h"
#include <QList>
#include <QTextStream>
#include <QtAlgorithms>
#include <QDateTime>
QTextStream cout(stdout);
QTextStream cin(stdin);


int getValue(QList<int> id)
{
    while (true) // Цикл продолжается до тех пор, пока пользователь не введет корректное значение
    {

        int a;
        std::cin >> a;
        std::cin.ignore(32767,'\n');

        if (std::cin.fail()) // если предыдущее извлечение оказалось провальным
        {
            cout<<"Error input"<<endl;
            std::cin.clear(); // возвращаем cin в 'обычный' режим работы
            std::cin.ignore(32767,'\n'); // и удаляем значения предыдущего ввода из входного буфера
            continue;
        }
        int i1 = id.indexOf(a);
        if (i1 == -1)
        {
            cout<<"No such sensor"<<endl;
            std::cin.clear(); // возвращаем cin в 'обычный' режим работы
            std::cin.ignore(32767,'\n');

            continue;
        }
        else // если всё хорошо, то возвращаем a
            return a;
    }
}

bool getDate(QString date)
{
    QDateTime dateToday=QDateTime::fromString(date,"dd.MM.yyyy HH:mm:ss");
    if (dateToday.isNull())
        return false;

}
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Base_date bd;
    bd.connect_db("C:\\Users\\maks\\cplusplus\\PanshinMaxim\\sensors.sqlite");
    QList<int> sen_id;
    bd.get_sensors(sen_id);


    while(1)
    {
        //выводим список датчиков

        std::cout<<"Choose sensor"<<std::endl;
        int i = 0;
        int s = sen_id.size();
        for(;i<s;i++)
        {
            std::cout<<"ID sensor: "<<sen_id[i]<<std::endl;
        }
        //проверка есть ли датчик
        int id = getValue(sen_id);


        //ввод даты и проверка
        std::cout<<"Data_beg: "<<std::endl;
        QString d_b=cin.readLine(); //дата начало
        if(!getDate(d_b))
        {
            cout<<"Error date"<<endl;
            continue;
        }
        std::cout<<"Data_end: "<<std::endl;
        QString d_e=cin.readLine(); //дата конец
        if(!getDate(d_e))
        {
            cout<<"Error date"<<endl;
            continue;
        }
        //вычисления
        cout<<"Sensor readings from "<<d_b<<"to"<<d_e<<endl;
        std::cout<<"Max temp: "<<bd.select_db_temp_max(id,d_b,d_e)<<"\t\t";
        std::cout<<"Max wet: "<<bd.select_db_wet_max(id,d_b,d_e)<<std::endl;
        std::cout<<"Min temp: "<<bd.select_db_temp_min(id,d_b,d_e)<<"\t\t";
        std::cout<<"Min wet: "<<bd.select_db_wet_min(id,d_b,d_e)<<std::endl;
        std::cout<<"avr temp: "<<bd.select_db_temp_arg(id,d_b,d_e)<<"\t\t";
        std::cout<<"avr wet: "<<bd.select_db_wet_arg(id,d_b,d_e)<<std::endl;
        std::cin.get();

        QProcess::execute("cmd /c cls");

    }
    return a.exec();
}
