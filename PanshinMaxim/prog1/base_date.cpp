#include "base_date.h"
#include <QtSql>
#include <QList>
#include <iostream>



Base_date::Base_date()
{

}

QSqlDatabase Base_date :: dbase = QSqlDatabase::addDatabase("QSQLITE");

int Base_date::connect_db(QString path)
{
     dbase.setDatabaseName(path);
      if (!dbase.open()) {
          qDebug() << "Что-то пошло не так!";
          return -1;
      }
      return 0;
}

int Base_date::insert_db(int ID,int temp,int wet,QString date_time)
{
    QSqlQuery my_query;
    my_query.prepare("INSERT INTO sensors_reading (ID, temperature, wet, date_time)"
                                  "VALUES (:ID, :temperature, :wet, :date_time);");
    my_query.bindValue(":ID",ID);
    my_query.bindValue(":temperature",temp);
    my_query.bindValue(":wet",wet);
    my_query.bindValue(":date_time",date_time);

    if(!my_query.exec())
    {
        qDebug()<<"Error insert"<<endl;
        qDebug()<< my_query.lastError();
        return -1;
    }
    return 0;
}
int Base_date::select_db_temp_max(int ID, QString date_time_beg, QString date_time_end)
{
    QSqlQuery my_query;
    my_query.prepare("SELECT temperature "
                       "FROM sensors_reading "
                       "WHERE date_time "
                       "BETWEEN :date_time_beg"
                       " AND :date_time_end"
                       " AND ID=:ID ORDER BY temperature");

    my_query.bindValue(":date_time_beg",date_time_beg);
    my_query.bindValue(":date_time_end",date_time_end);
    my_query.bindValue(":ID",ID);

    if(!my_query.exec())
    {
        qDebug()<<"Error select"<<endl;
        qDebug()<<my_query.lastError();

    }
    my_query.last();

    int a=my_query.value(0).toInt();

    return a;
}

int Base_date::select_db_wet_max(int ID, QString date_time_beg, QString date_time_end)
{
    QSqlQuery my_query;
    my_query.prepare("SELECT wet "
                       "FROM sensors_reading "
                       "WHERE date_time "
                       "BETWEEN :date_time_beg"
                       " AND :date_time_end"
                       " AND ID=:ID ORDER BY wet"
                       );

    my_query.bindValue(":date_time_beg",date_time_beg);
    my_query.bindValue(":date_time_end",date_time_end);
    my_query.bindValue(":ID",ID);


    if(!my_query.exec())
    {
        qDebug()<<"Error select"<<endl;
        qDebug()<<my_query.lastError()<<endl;

    }
    my_query.last();

    int result_max=my_query.value(0).toInt();


    return result_max;

}

void Base_date::get_sensors(QList<int> &sen)
{
    QSqlQuery query("SELECT ID FROM sensors");
    while (query.next()) {
             int ID = query.value(0).toInt();
             sen<<ID;
    }
}

double Base_date::select_db_wet_arg(int ID,QString date_time_beg,QString date_time_end)
{
    QSqlQuery my_query;
    my_query.prepare("SELECT wet "
                       "FROM sensors_reading "
                       "WHERE date_time "
                       "BETWEEN :date_time_beg"
                       " AND :date_time_end"
                       " AND ID=:ID"
                       );

    my_query.bindValue(":date_time_beg",date_time_beg);
    my_query.bindValue(":date_time_end",date_time_end);
    my_query.bindValue(":ID",ID);


    if(!my_query.exec())
    {
        qDebug()<<"Error select"<<endl;
        qDebug()<<my_query.lastError()<<endl;

    }
    int size=0;
    double result=0;
    while(my_query.next())
    {
        size++;
        result+=my_query.value(0).toDouble();
    }
    return result/size;
}

double Base_date::select_db_temp_arg(int ID,QString date_time_beg,QString date_time_end)
{
    QSqlQuery my_query;
    my_query.prepare("SELECT temperature "
                       "FROM sensors_reading "
                       "WHERE date_time "
                       "BETWEEN :date_time_beg"
                       " AND :date_time_end"
                       " AND ID=:ID"
                       );
    my_query.bindValue(":date_time_beg",date_time_beg);
    my_query.bindValue(":date_time_end",date_time_end);
    my_query.bindValue(":ID",ID);


    if(!my_query.exec())
    {
        qDebug()<<my_query.lastError()<<endl;

    }
    int size=0;
    double result=0;
    while(my_query.next())
    {
        size++;
        result+=my_query.value(0).toDouble();

    }
    return result/size;
}

int Base_date:: select_db_wet_min(int ID,QString date_time_beg,QString date_time_end)
{
    QSqlQuery my_query;
    my_query.prepare("SELECT wet "
                       "FROM sensors_reading "
                       "WHERE date_time "
                       "BETWEEN :date_time_beg"
                       " AND :date_time_end"
                       " AND ID=:ID ORDER BY wet"
                       );

    my_query.bindValue(":date_time_beg",date_time_beg);
    my_query.bindValue(":date_time_end",date_time_end);
    my_query.bindValue(":ID",ID);


    if(!my_query.exec())
    {
        qDebug()<<"Error select"<<endl;
        qDebug()<<my_query.lastError()<<endl;

    }
    my_query.first();

    int result_max=my_query.value(0).toInt();
    return result_max;
}

int Base_date::select_db_temp_min(int ID,QString date_time_beg,QString date_time_end)
{
    QSqlQuery my_query;
    my_query.prepare("SELECT temperature "
                       "FROM sensors_reading "
                       "WHERE date_time "
                       "BETWEEN :date_time_beg"
                       " AND :date_time_end"
                       " AND ID=:ID ORDER BY temperature"
                       );

    my_query.bindValue(":date_time_beg",date_time_beg);
    my_query.bindValue(":date_time_end",date_time_end);
    my_query.bindValue(":ID",ID);


    if(!my_query.exec())
    {
        qDebug()<<my_query.lastError()<<endl;

    }
    my_query.first();

    int result_max=my_query.value(0).toInt();

    return result_max;
}
