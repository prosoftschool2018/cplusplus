#include "sensor.h"
#include <QDateTime>
#include <QDebug>

Sensor::Sensor()
{

}

int Sensor::get_temp()
{
    return (qrand() % ((40 + 1) - (-30)) + (-30));
}

int Sensor::get_wet()
{
    return (qrand() % ((75 + 1) - (40)) + (40));
}

QString Sensor::get_data_time()
{
    QDateTime dateToday = QDateTime::currentDateTime();

    return dateToday.toString("dd.MM.yyyy HH:mm:ss");

}
