#include <QCoreApplication>
#include <QtSql>
#include<QString>
#include <QTextStream>
#include "base_date.h"
#include "sensor.h"
#include <QList>
#include <QtTest/QTest>

QTextStream cout(stdout);
QTextStream cin(stdin);



int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Sensor s; //объект имитирует датчик
    Base_date bd; //работа с бд
    bd.connect_db("C:\\Users\\maks\\cplusplus\\PanshinMaxim\\sensors.sqlite");
    QList<int> s_id;//для хранение id датчиков

    //получает ID с бд
    bd.get_sensors(s_id);

    int size=s_id.size();
    int i=0;
    //имитируем и записываем в бд
    while(1)
    {
        if(i>size-1)
        {
            i=0;
        }
        bd.insert_db(s_id[i],s.get_temp(),s.get_wet(),s.get_data_time());
        cout<<"received from:"<<s_id[i]<<endl;
        i++;
        QTest::qWait(333);

    }
    return a.exec();
}
