#ifndef SENSOR_H
#define SENSOR_H
#include<QString>


class Sensor
{
public:
    Sensor();
    int get_temp();
    int get_wet();
    QString get_data_time();

};

#endif // SENSOR_H
