#ifndef BASE_DATE_H
#define BASE_DATE_H
#include<QString>
#include <QtSql>
#include<QList>


class Base_date
{
public:
    Base_date(); 
    int insert_db(int ID,int temp,int wet,QString date_time);
    int connect_db(QString path);
    int select_db_temp_max(int ID,QString date_time_beg,QString date_time_end);

    int select_db_wet_max(int ID,QString date_time_beg,QString date_time_end);

    int select_db_wet_min(int ID,QString date_time_beg,QString date_time_end);

    int select_db_temp_min(int ID,QString date_time_beg,QString date_time_end);

    double select_db_wet_arg(int ID,QString date_time_beg,QString date_time_end);

    double select_db_temp_arg(int ID,QString date_time_beg,QString date_time_end);

    void get_sensors(QList<int> &sen);

    static QSqlDatabase dbase;

};

#endif // BASE_DATE_H
